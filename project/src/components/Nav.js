import React from 'react';
import '../styles/style.scss'

function Nav() {
  return (

    <div>
        {/*<script src="https://kit.fontawesome.com/e5022fe411.js" crossOrigin="anonymous"></script>*/}
        <div className="navbar-container">
            <nav className="navbar">
                <ul className="navbar-nav">
                    <li className={"nav-item logo link-text"}><span>W.Toimi</span></li>
                    <li className="nav-item">
                        <a href="/" className="nav-link">
                            <span className="link-text">background</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a href="/" className="nav-link">
                            <span className="link-text">programming</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a href="/" className="nav-link">
                            <span className="link-text">webDev</span>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a href="/" className="nav-link">
                            <span className="link-text">contact</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>



      
    </div>
  );
}

export default Nav;
