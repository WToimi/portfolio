import React from 'react';
import '../styles/style.scss'

function Slideshow() {
    return (

        <div>
            <div className={"slideshow-container"}>
                <div className={"slideshow-item"}>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt qui quibusdam similique ullam! Aperiam eaque illum natus, odio repudiandae velit?
                </div>
                <div className={"slideshow-item"}>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium blanditiis est facere facilis modi nobis optio pariatur vero. Odio, veniam.
                </div>
                <div className={"slideshow-item"}>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid blanditiis delectus error est, expedita magnam nisi officiis soluta voluptatibus! Nihil.
                </div>
            </div>

        </div>
    );
}

export default Slideshow;
