import React from 'react';
import ReactDOM from 'react-dom';
import Slideshow from './components/Slideshow';
import Nav from './components/Nav';

ReactDOM.render(
  <React.StrictMode>
    <Nav />
    <Slideshow />
  </React.StrictMode>,
  document.getElementById('root')
);

